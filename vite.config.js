// 导入必要的模块
import { defineConfig } from 'vite';
import { fileURLToPath } from 'url';
import eslintPlugin from 'vite-plugin-eslint';
import Components from 'unplugin-vue-components/vite';
import AutoImport from 'unplugin-auto-import/vite';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';
import sass from 'sass';


import vue from '@vitejs/plugin-vue'; // 如果使用 Vue，需要导入 Vue 插件
// 可能还有其他插件，如 CSS 预处理器的插件等

export default defineConfig({
  // Vite 应用的基本配置
  root: './', // 项目根目录，默认是项目根目录
  base: './', // 部署应用的基本路径，默认是 './'，用于生成的静态资源路径前缀
  publicDir: 'public', // 公共静态资源目录，默认是 'public'
  assetsInclude: ['**/*.ext'], // 指定额外的静态资源文件夹或文件类型

  // 服务器配置
  server: {
    host: 'localhost', // 服务器主机，默认是 localhost
    port: 3000, // 服务器端口，默认是 3000
    open: false, // 服务器启动时自动打开浏览器，默认是 false
    https: false, // 是否使用 HTTPS，默认是 false
    cors: true, // 开启 CORS，默认是 true
    proxy: { // 代理配置，用于 API 请求转发
      '/apiApi': {
        // target: 'https://www.hao123.com/api/getgoodthing?pageSize=3',
        target: 'https://www.fastmock.site/mock/f508358b65c1868607c2b869ddd08e12/api/',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/apiApi/, '/apiApi'),
      },
    },
  },
  // 构建配置
  build: {
    outDir: 'dist', // 构建输出目录，默认是 'dist'
    assetsDir: 'assets', // 静态资源输出目录，默认是 ''
    rollupOptions: {}, // Rollup 打包的额外选项
    minify: 'terser', // 压缩代码的工具，默认是 terser
    sourcemap: true, // 是否生成 source map，默认是 false 生产环境下，true 开发环境下
  },

  // 预构建依赖配置
  optimizeDeps: {
    include: ['vue', 'vue-router'], // 需要预构建的依赖
    exclude: [], // 排除的依赖
  },

  // 插件配置
  plugins: [
    /*   {
        ...vue(),
        enforce: 'post',
        apply: 'build'

        // apply: ''build' 或'serve''
        // enforce: 'pre 或 post'

      } */

    vue(),

    // ESlint 插件，用于在开发时检查 JavaScript 和 Vue 文件的语法错误
    // eslintPlugin({
    //   include: ['src/**/*.js', 'src/**/*.vue'],
    //   exclude: ['node_modules/**'],
    //   throwOnError: true, // 遇到错误时抛出异常
    //   throwOnWarning: false, // 遇到警告时不抛出异常（保持默认）
    // }),
    // 自动导入 Vue 和 Element Plus 组件
    Components({
      resolvers: [ElementPlusResolver()],
    }),

    // 自动导入 Vue、Pinia、Vue Router 等库的功能
    AutoImport({
      imports: [
        'vue',
        'pinia',
        'vue-router',
      ],
      dirs: ['src/hooks'], // 自动导入自定义 hooks
      resolvers: [],
      dts: 'auto-imports.d.ts', // 生成类型声明文件
    }),
  ],

  // 解析器选项
  resolve: {
    alias: { // 别名配置，方便导入模块
      '@': fileURLToPath(new URL('./src', import.meta.url)),
    },
    extensions: ['.js', '.ts', '.jsx', '.tsx', '.json'], // 自动解析的文件扩展名
  },

  // CSS 配置
  css: {
    preprocessorOptions: { // CSS 预处理器选项
      scss: {
        implementation: sass,
        // additionalData: `@import "./src/styles/variables.scss";`, // 全局变量
      },
    },
  },

  // 更多配置...
  // ...
});