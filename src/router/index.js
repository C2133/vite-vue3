import { createMemoryHistory, createRouter } from 'vue-router'
import { defineComponent } from 'vue'

// import HomeView from '@/view/1.vue'
import HomeView from '@/view/3.vue'
import AboutView from '@/view/2.vue'

const routes = [
  // { path: '/', component: HomeView },
  // { path: '/about', component: AboutView },
  { path: '/', component: defineComponent(HomeView) },
  { path: '/about', component: defineComponent(AboutView) },
]

const router = createRouter({
  history: createMemoryHistory(),
  routes,
})
export default router