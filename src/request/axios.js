// src/request.js
import axios from 'axios';
import { ElMessage } from 'element-plus';
console.log(import.meta.env.BASE_URL, import.meta.env);
// 创建 axios 实例
const service = axios.create({
  baseURL: import.meta.env.BASE_URL, // 应用的基本 API 地址
  timeout: 15000, // 请求超时时间
});

// 请求拦截器
service.interceptors.request.use(
  config => {
    // 在发送请求之前做些什么，例如添加 token
    return config;
  },
  error => {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 响应拦截器
service.interceptors.response.use(
  response => {
    const res = response.data;
    if (res.code !== 200) {
      ElMessage.error(res.message || 'Error');
      return Promise.reject(new Error(res.message || 'Error'));
    }
    return res;
  },
  error => {
    ElMessage.error(error.message || '网络错误');
    return Promise.reject(error);
  }
);

export default service;