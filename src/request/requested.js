// src/apis/index.js
import service from './axios';

// 封装 GET 请求
const get = (url, params = {}) => {
  return service.get(url, { params });
}

// 封装 POST 请求
const post = (url, data = {}) => {
  return service.post(url, data);
}

// 封装 PUT 请求
const put = (url, data = {}) => {
  return service.put(url, data);
}

// 封装 DELETE 请求
const del = (url, params = {}) => {
  return service.delete(url, { params });
}

// 封装 PATCH 请求
const patch = (url, data = {}) => {
  return service.patch(url, data);
}

export {
  get,
  post,
  put,
  del,
  patch
}