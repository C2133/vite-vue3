import { createApp } from 'vue'
import { createPinia } from 'pinia'
import './style.css'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import router from './router'
import "@/mock/index";


const pinia = createPinia()
const app = createApp(App)

console.log(app.config.globalProperties,'config ');

app.config.globalProperties.$LX = 'LX'

app.use(pinia)
app.use(router)
app.use(ElementPlus, { size: 'small', zIndex: 3000, locale: zhCn, })
app.mount('#app')
